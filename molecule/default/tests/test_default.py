import os
import yaml
import pytest
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")


def container(name):
    return "monitoring_{}_1".format(name)


@pytest.fixture(scope="module")
def AnsibleVars(host):
    stream = host.file("/tmp/ansible-vars.yml").content
    return yaml.safe_load(stream)


def test_running_base(host, AnsibleVars):
    with host.sudo():
        for name in ["grafana", "prometheus", "alertmanager", "cadvisor"]:
            c = host.docker(container(name))
            assert c.is_running
            inspect = c.inspect()
            assert name == name and inspect["State"]["Status"] == "running"
