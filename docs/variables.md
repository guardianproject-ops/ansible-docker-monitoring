## Role Variables

* `monitoring_config_local_dir`: `` - REQUIRED. the local directory that contains prometheus and alert manager configuration.



* `monitoring_listen_address`: `` - REQUIRED. the ip address of a local interface that the services will be exposed on (could be a wireguard/vpn address, or just a local internal iface address)



* `monitoring_domain`: `` - REQUIRED.  the fqdn that grafana will be exposed at



* `monitoring_letsencrypt_email`: `` - REQUIRED. the email address used in the letsencrypt acme account



* `monitoring_config_prometheus_dir`: `/etc/monitoring/prometheus` - the path on the remote host where the config will be stored (outside a docker volume)



* `monitoring_smtp_user`: `` - REQUIRED.



* `monitoring_smtp_password`: `` - REQUIRED.



* `monitoring_smtp_from_domain`: `` - REQUIRED.



* `monitoring_smtp_host_port`: `587` - 



* `monitoring_config_alertmanager_dir`: `/etc/monitoring/alertmanager` - the path on the remote host where the config will be stored (outside a docker volume)



* `monitoring_config_grafana_provisioning_dir`: `/etc/monitoring/grafana/provisioning` - the path on the remote host where the config will be stored (outside a docker volume)



* `monitoring_grafana_user`: `admin` - the grafana web ui user name



* `monitoring_grafana_pass`: `` - REQUIRED. the grafana web ui user password



* `monitoring_back_net_subnet`: `'172.50.0.0/16'` - the internal network cidr for the monitoring services this is only for  prom, alertmanager, etc, not your exporters



* `monitoring_back_net_iprange`: `'172.50.0.0/24'` - the internal network range for the monitoring services this is only for  prom, alertmanager, etc, not your exporters



* `monitoring_prometheus_external_url`: `` - REQUIRED. the url at which prometheus will be available



* `monitoring_alertmanager_external_url`: `` - REQUIRED. the url at which alertmanager will be available



* `monitoring_matrix_enabled`: `false` - whether to enable the matrix chat bot for alertmanager



* `monitoring_matrix_shared_secret`: `` - required when the matrix chatbot is enabled



* `monitoring_matrix_room`: `` - the fully qualifie matrix room id. required when the matrix chatbot is enabled



* `monitoring_matrix_user`: `` - the fully qualified id of the matrix chatbot user. required when the matrix chatbot is enabled



* `monitoring_matrix_token`: `` - the token of the matrix chatbot user. required when the matrix chatbot is enabled



* `monitoring_matrix_homeserver`: `https://matrix.org` - the matrix homeserver to use. required when the matrix chatbot is enabled



* `monitoring_sigarillo_secrets`: `` - required when sigarillo is enabled



* `monitoring_sigarillo_db_pass`: `` - required when sigarillo is enabled


